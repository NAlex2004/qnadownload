#ifndef DOWNLOADSORTFILTERMODEL_H
#define DOWNLOADSORTFILTERMODEL_H
#include <QSortFilterProxyModel>
#include "downloadfiledata.h"
#include <QSet>

typedef QSet<DFState> DFStateSet;

class DownloadSortFilterModel : public QSortFilterProxyModel
{
public:
    DownloadSortFilterModel(QObject *parent = 0);
    ~DownloadSortFilterModel();


    // QSortFilterProxyModel interface
    DFStateSet getFilterSet() const;
    void setFilterSet(const DFStateSet &value);
    void clearFilter();
    virtual DownloadFileData*  getRowData(int sourceRow) const;
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

private:
    DFStateSet filterSet;
};

#endif // DOWNLOADSORTFILTERMODEL_H
