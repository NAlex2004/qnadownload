#ifndef DOWNLOADTIMEDELEGATE_H
#define DOWNLOADTIMEDELEGATE_H

#include <QStyledItemDelegate>

class DownloadTimeDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    DownloadTimeDelegate(QObject *parent = 0);

    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // DOWNLOADTIMEDELEGATE_H
