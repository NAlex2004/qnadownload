#ifndef HTTPGET_H
#define HTTPGET_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFile>
#include <QPointer>
#include <QTime>
#include <QTimer>

#define SECS_FOR_SPEED 1
#define TIMEOUT 8000
#define TIMEOUT_COUNTER_INTERVAL 1000
//сколько раз таймер вызвался, а прогресса не было
#define IDLE_TIMEOUT_INTERVALS 5
//отменить при отсутствии резутьтата TIMEOUT_COUNTER_INTERVAL * INTERVALS_TO_ABORT / 1000 сек
#define INTERVALS_TO_ABORT 30

typedef struct
{
    QUrl url;
    qint64 startFrom;
    qint64 bytesCount;
} DownloadData;

class HttpGet : public QObject
{
    Q_OBJECT
public:
    explicit HttpGet(QObject *parent = 0);
    ~HttpGet();
    //bytesCount == 0 - скачать все, или столько байт, startFrom игнорируется, если  appendToExisting == true
    //и файл существует
    bool getFile(QUrl url, QString localPath, bool appendToExisting = true, qint64 startFrom=0, qint64 bytesCount=0);
    //если уже начинали загрузку, продолжить с теми же параметрами
    bool reGetFile();

    bool isDownloading() const;
    bool isAborting() const;
    void abortDownload();
    //получить размер файла с сервера
    qint64 getFileSize(QUrl url);
    //получить сохраненный размер файла на сервере
    qint64 getRemoteFileSizeSaved();
    qint64 getDownloadingFileSize() const;
    uchar getPercent() const;
    qint64 getBytesReceived() const;
    uint getLastErrCode() const;
    QString getLastErrMsg() const;
    int getSpeed() const;
    bool canResume() const;    
    DownloadData getDownloadData() const;
    //остановить, когда получим byte байт (размер файла такой). false если уже получено больше,
    //тогда до конца качаем
    // А, не, если что, файл обрежем просто!
    bool setStopOnSize(qint64 fSize);
    QString getFilePath() const;
    QString getFileName() const;
    //true, если получено все, что нужно, качать больше нечего
    bool gotAll() const;
    int getLastReplyCode() const;
    bool hasStarted();
    int getIdleIntervals();
protected:
    uint errCode;
    QString errMsg;
    uchar percent;
    qint64 remoteFileSize;
    qint64 gotBytes;
    qint64 lastGotBytes;
    QTime lastTime;
    QTime curTime;
    bool downloading;
    QString filePath;
    QFile file;    
    bool appentToFile;
    // Kb/s
    int speed;
    qint64 stopOnSize;
    //сохраняем заданные при первом вызове getFile значения для последующего возобновления без их передачи
    DownloadData downloadData;
    //устанавливается в true после первого вызова getFile, т.е. можно тогда возобновлять
    //без указаний параметров
//    bool getFileCalled;
private:
    QNetworkAccessManager manager;
    QPointer<QNetworkReply> reply;
    QPointer<QNetworkReply> sReply;
    QTimer timer;

    bool rangeSupported;
    bool fromReGet;
    bool gotEverything;
    bool needAbort;
    bool started;
    int lastReplyCode;
    int idleIntervalsCounter;

    void calcPercent();       
    //bool isGetSizeActive;
signals:
    void netError(QNetworkReply::NetworkError code, QString strError);
    void localError(QString errMsg);
    void finished();
    void downloadFileProgress(qint64 gotBytes, qint64 totalBytes, uchar percent, int speed);
    void replyCode(int code, QString msg);
    void downloadStarted(bool resumeSupported);
    void redirectToURL(QString newURL);
public slots:

protected slots:
    void downloadFinished();
    void error(QNetworkReply::NetworkError code);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void readData();
    void onMetadataChanged();
    void onNeedAbort();
    void onGetFileSizeTimeout();
    void onCheckIdleTimer();
};

#endif // HTTPGET_H
