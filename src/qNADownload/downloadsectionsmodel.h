#ifndef DOWNLOADSECTIONSMODEL_H
#define DOWNLOADSECTIONSMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include "httpmultiget.h"


//typedef QList<HttpGet*> SectionList;

class DownloadSectionsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    static const uchar COLCOUNT = 4;
    explicit DownloadSectionsModel(const SectionList* secList = 0, QObject *parent = 0);
    void setSectionList(const SectionList* secList);
    void refresh();
private:
    const SectionList *parts;

signals:

public slots:

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;
};

#endif // DOWNLOADSECTIONSMODEL_H
