#include "downloadtablemodel.h"


DownloadTableModel::DownloadTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{

}

DownloadTableModel::~DownloadTableModel()
{
    foreach (QPointer<DownloadFileData> fData, fileList)
    {
        if (fData)
            fData->deleteLater();
//            delete fData;
    }
}

void DownloadTableModel::rowDataChanged()
{
    QPointer<DownloadFileData> downloader = qobject_cast<DownloadFileData*>(sender());
    int row = fileList.indexOf(downloader);
    if (row >= 0)
        emit dataChanged(index(row, 0), index(row, COL_COUNT - 1));
}

void DownloadTableModel::timeChanged()
{
    QPointer<DownloadFileData> downloader = qobject_cast<DownloadFileData*>(sender());
    int row = fileList.indexOf(downloader);
    if (row >= 0)
        emit dataChanged(index(row, 5), index(row, 5));

}

void DownloadTableModel::stateChanged()
{
    QPointer<DownloadFileData> downloader = qobject_cast<DownloadFileData*>(sender());
    int row = fileList.indexOf(downloader);
    if (row >= 0)
        emit dataChanged(index(row, 0), index(row, 0));
}


int DownloadTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return fileList.size();
}

int DownloadTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return COL_COUNT;
    /* columns:
     *state (icon), name or path
     *bytes done
     *size
     *percent (progress)
     *speed
     *time elapsed
     *time left
     */
}

QVariant DownloadTableModel::data(const QModelIndex &index, int role) const
{
    if ((!index.isValid()) || (index.row() < 0) || (index.row() >= fileList.size()))
        return QVariant();

    QPointer<DownloadFileData> fData = fileList[index.row()];

    if (role == Qt::DisplayRole)
    {        
        switch (index.column())
        {
        case 0:
            return fData->getFileName();
        case 1:
        {
            qint64 bytesDone = fData->getBytesDone();
            if (bytesDone < 1024)
                return QString(trUtf8("%1 б")).arg(bytesDone);
            return QString(trUtf8("%1 Кб")).arg(bytesDone / 1024);
        }
        case 2:
        {
            qint64 fileSize = fData->getFileSize();
            if (fileSize < 1024)
                return QString(trUtf8("%1 б")).arg(fileSize);
            return QString(trUtf8("%1 Кб")).arg(fileSize / 1024);
        }
        case 3:
            return fData->getPercent();
        case 4:
            return QString(trUtf8("%1 Кб/с")).arg(fData->getSpeed());
        case 5:
            return fData->getTimeElapsed();
        case 6:
            return fData->getTimeLeft();
        case STATE_COLUMN:
            return QVariant((uint)fData->getState());
        default:
            return QVariant();
        }
    }

    if (role == Qt::TextAlignmentRole)
    {
        if ((index.column() == 1) || (index.column() == 2) || (index.column() == 4))
            return QVariant(Qt::AlignCenter | Qt::AlignVCenter);
    }

    if (role == Qt::ToolTipRole)
    {
        QString toolTip = trUtf8("Состояние: ") + fData->getStateStr() + "\n\n" +
                trUtf8("Источник: ") + fData->getURL() + "\n" +
                trUtf8("Получатель: ") + fData->getLocalPath() + "\n\n" +
                trUtf8("Получено: %1 Кб из %2 Кб").arg(fData->getBytesDone() / 1024).arg(fData->getFileSize() / 1024);
        return toolTip;
    }

    return QVariant();
}

QVariant DownloadTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ((orientation == Qt::Horizontal) && (role == Qt::DisplayRole))
    {
        if (section >= 0 && section < COL_COUNT)
            return headers[section];
    }
    return QVariant();
}

bool DownloadTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);

    int startRow = row;
    int endRow = row + count - 1;
    int maxRow = fileList.size() - 1;

    if (maxRow < 0)
        return false;

    startRow = qBound(0, startRow, maxRow);
    endRow = qBound(0, endRow, maxRow);
    if (startRow > endRow)
    {
        int i = endRow;
        endRow = startRow;
        startRow = i;
    }

    count = endRow - startRow + 1;
    beginRemoveRows(parent, startRow, endRow);
    QPointer<DownloadFileData> fData;
    for (int i=0; i<count; i++)
    {
        fData = fileList[startRow];
        if (fData)
        {
            fData->deleteDownload(false);
            fData->deleteLater();
//            delete fData;
        }
        fileList.removeAt(startRow);
    }
    endRemoveRows();
    return true;
}

bool DownloadTableModel::appendRow(QPointer<DownloadFileData> const fileData)
{
    if (!fileData)
        return false;
    beginInsertRows(QModelIndex(), fileList.size(), fileList.size());
    fileList.append(fileData);
    endInsertRows();
    return true;
}

Qt::ItemFlags DownloadTableModel::flags(const QModelIndex &index) const
{
    if (index.isValid() && index.row() >= 0 && index.row() < fileList.size())
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;

    return Qt::NoItemFlags;
}

void DownloadTableModel::setRowData(int row, const QPointer<DownloadFileData> fileData)
{
    if (!fileData || row < 0 || row >= fileList.size())
        return;
    fileList.replace(row, fileData);
    emit dataChanged(index(row, 0), index(row, COL_COUNT - 1));
}

QPointer<DownloadFileData> DownloadTableModel::getRowData(int row) const
{
    if (row < 0 || row >= fileList.size())
        return 0;//nullptr;
    return fileList.at(row);
}
