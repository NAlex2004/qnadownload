#ifndef DOWNLOADPROGRESSDELEGATE_H
#define DOWNLOADPROGRESSDELEGATE_H

#include <QStyledItemDelegate>


class DownloadProgressDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit DownloadProgressDelegate(QObject *parent = 0);

signals:

public slots:


    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // DOWNLOADPROGRESSDELEGATE_H
