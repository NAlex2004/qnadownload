#include "adddialog.h"
#include "ui_adddialog.h"
#include <QFileDialog>
#include <QUrl>
#include <QMessageBox>
#include <QFileInfo>
#if QT_VERSION >= 0x050000
    #include <QStandardPaths>
#else
    #include <QDesktopServices>
#endif

AddDialog::AddDialog(QWidget *parent, CheckDelegate fileExistsDelegate, QModelIndex selected) :
    QDialog(parent),
    ui(new Ui::AddDialog)//, fileExists(fileExistsDelegate)
{
    ui->setupUi(this);
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(onAccepted()));    
    fileExists = fileExistsDelegate;
    selectedIndex = selected;
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::setURL(const QString URL)
{
    ui->leURL->setText(URL);
}

void AddDialog::getResults(QUrl &URL, QString &localPath, uchar &threadCount, bool &runImmediate)
{
    URL.setUrl(ui->leURL->text());
    localPath = QDir::toNativeSeparators(QDir(ui->leDir->text() +
                                              "/" + ui->leFileName->text()).absolutePath());
    threadCount = ui->sbThreads->value();
    runImmediate = ui->cbRunImmediate->isChecked();
}


int AddDialog::exec()
{
    on_leURL_editingFinished();
    QString path;
#if QT_VERSION >= 0x050000
    path = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
#else
    path = QDesktopServices::storageLocation(QDesktopServices::HomeLocation);
#endif
    ui->leDir->setText(QDir::toNativeSeparators(path));
    return QDialog::exec();
}

int AddDialog::exec(const QString initialDir)
{
    if (initialDir.isEmpty() || !QDir(initialDir).exists())
        return exec();
    on_leURL_editingFinished();
    ui->leDir->setText(QDir::toNativeSeparators(initialDir));
    return QDialog::exec();
}

int AddDialog::exec(const QString URL, const QString localPath, const uchar threads, const bool isRunImmediateVisible)
{
    ui->leURL->setText(URL);

    QFileInfo fi(localPath);
    ui->leDir->setText(QDir::toNativeSeparators(fi.path()));
    ui->leFileName->setText(fi.fileName());
    ui->sbThreads->setValue(threads);
    ui->cbRunImmediate->setVisible(isRunImmediateVisible);
    return QDialog::exec();
}

void AddDialog::on_toolButton_clicked()
{
    QString dir = ui->leDir->text();
//    QFileDialog dirDialog(this, trUtf8("Выбор каталога для сохранения."), dir);
//    dirDialog.setFileMode(QFileDialog::Directory);
//    dirDialog.setOptions(QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks |
//                         QFileDialog::DontUseNativeDialog);

//    if (dirDialog.exec())
//    {
//        QStringList sel = dirDialog.selectedFiles();
//        if (!sel.isEmpty())
//            ui->leDir->setText(QDir::toNativeSeparators(sel.at(0)));
//    }


    QString resDir = QFileDialog::getExistingDirectory(this, trUtf8("Выбор каталога для сохранения."), dir);
    if (!resDir.isEmpty())
        ui->leDir->setText(QDir::toNativeSeparators(resDir));
}

void AddDialog::on_leURL_editingFinished()
{
    QUrl URL(ui->leURL->text());
    if (!URL.isEmpty())
    {
#if QT_VERSION >= 0x050000
        ui->leFileName->setText(URL.fileName());
#else
        ui->leFileName->setText(QFileInfo(URL.toString()).fileName());
#endif
    }
}

void AddDialog::onAccepted()
{
    QString dir(ui->leDir->text());

    try
    {
        QUrl url(ui->leURL->text());
        if (!url.isValid())
            throw trUtf8("Не верный адрес!");

        if (dir.isEmpty())
            throw trUtf8("Каталог не выбран!");

        if (!QDir(dir).exists())
        {
            throw trUtf8("Каталог не существует."), trUtf8("Каталог\n") + dir +
                    trUtf8("\nНе существует!");
        }

        if (ui->leFileName->text().isEmpty())
            throw trUtf8("Не указано имя файла!");
        QString path = QDir::toNativeSeparators(QDir(ui->leDir->text() +
                                                     "/" + ui->leFileName->text()).absolutePath());
        if (fileExists != 0)
            if (fileExists(path, selectedIndex))
            {
                throw trUtf8("Файл или закачка существует!");
            }

    }
    catch (QString errMsg)
    {
        QMessageBox::critical(this, trUtf8("Ошибка."), errMsg);
        return;
    }

    accept();
}

