#ifndef HTTPMULTIGET_H
#define HTTPMULTIGET_H

#include <QString>
//#include <QDateTime>
#include <QObject>
#include <QList>
#include <QTimer>
#include <QPointer>
#include "httpget.h"

const int PROGRESS_INTERVAL = 500; //интервал между сигналами downloadProgress
const uchar MAX_THREADS = 10;
const uint MIN_SIZE_FOR_MULTI = 1024 * 1024 * 5; //5 Mb
const uint INTERVAL = 5000; //таймер раз в 5 сек для перезапуска отвалившихся
const uint BUFF_SIZE = 2048;//размер буфера для соединения файлов

typedef enum {dfUnknown, dfDownloading, dfDone, dfError, dfPaused} DFState;
typedef QList<QPointer<HttpGet> > SectionList;

class HttpMultiGet : public QObject
{
    Q_OBJECT
public:
    HttpMultiGet(QString URLPath = "", QString localFile = "", uchar threads = 1, QObject *parent = 0);
    ~HttpMultiGet();

    QString getLocalPath() const;
    bool setLocalPath(const QString &value);
    QString getFileName() const;

    uchar getPercent() const;
    qint64 getFileSize() const;
    qint64 getBytesDone() const;
    uchar getThreadCount() const;
    //время в секундах
    uint getTimeElapsed() const;
    uint getTimeLeft() const;
    DFState getState() const;
    QString getStateStr() const;
    QString getTimeElapsedStr() const;

    //эти убрать
    void setPercent(uchar value);
    void setState(DFState value);
    void setTimeElapsed(uint value);
    void setBytesDone(qint64 value);

    //при старте, если размер не совпадает, качать заново
    void setRemoteFileSize(qint64 value = 0);

    int getSpeed() const;
    const SectionList * getSectionList();

    QString getURL() const;
    bool setURL(const QString &value);
    void setMaxThreads(uchar threads);
    uchar getMaxThreads();

    bool start(bool dontGetNewFileSize = false);
    bool restart();
    void stop();
    bool resume();

    //удаление секций. если true, то и файла
    void deleteDownload(bool deleteFile = false);
    void deleteLocalFile();

    void setDeleteOnDestroy(const bool delParts = false, const bool delLocalFile = false);

protected:
    DFState state;
    QString localPath;
    QString URL;
    uchar percent;
    qint64 fileSize;
    qint64 bytesDone;
    uchar threadCount;
    uchar errThreadCount;
    uchar finishedThreadCount;
    int speed;
    //в секундах
    uint timeElapsed;
//    QTime timeLeft;
    QList<QPointer<HttpGet> > parts;

protected slots:
    void onNetworkError(QNetworkReply::NetworkError code, QString strError);
    void onFinished();
    void onDownloadProgress(qint64 gotBytes, qint64 totalBytes, uchar percent, int speed);
    void onLocalError(QString errMsg);
    void onReplyCode(int code, QString msg);
    void onTimeCounter();
    void onAbortTimer();
    void onTimer();
    void onRedirectToURL(QString newURL);

private:
    static const QSet<QNetworkReply::NetworkError> criticalErrorSet;
    static const QString strStates[5];
    QTimer timer;
    QTimer timeCounter;
    QTimer abortTimer;
    QTime lastTimePr;
    QTime curTimePr;
    uchar maxThreads;
    bool starting;
    bool deleteFileOnDestroy;
    bool deletePartsOnDestroy;

    void clearParts();
    void deleteFileParts();
    void updateState();
    void startTimers();
    void stopTimers();
    void concatFiles();

signals:
    void downloadProgress(qint64 gotBytes, qint64 totalBytes, uchar percent, int speed);
    void allFinished();
    void error(QString errMsg);
    void started(bool canResume);
    void stopped();
    void stateChanged(const DFState newState);
    void replyCode(int code, QString msg);
    void timeCounterSignal(uint elapsedSeconds);
};


#endif // HTTPMULTIGET_H
