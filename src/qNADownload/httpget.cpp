#include "httpget.h"
#include <QApplication>
#include <QtDebug>
#include <QFileInfo>
#include <QDir>
#include <QTimer>


HttpGet::HttpGet(QObject *parent) : QObject(parent),
    errCode(0), errMsg(""), percent(0), remoteFileSize(0), gotBytes(0), lastGotBytes(0),
    downloading(false), filePath(""), appentToFile(false), speed(0),
    stopOnSize(0), reply(0), rangeSupported(false), fromReGet(false), gotEverything(false),
    needAbort(false), started(false), lastReplyCode(0), idleIntervalsCounter(0)
{
    downloadData.startFrom = 0;
    downloadData.bytesCount = 0;
    downloadData.url.setUrl("");
    qDebug() << trUtf8("Создана секция");
    timer.setInterval(TIMEOUT_COUNTER_INTERVAL);
    connect(&timer, SIGNAL(timeout()), this, SLOT(onCheckIdleTimer()));
}

HttpGet::~HttpGet()
{
    if (downloading)
    {
        if ((!needAbort) && reply)
            reply->abort();        
    }
    if (reply)
        reply->deleteLater();
    qDebug() << trUtf8("Удаляем секцию с ") + getFileName();
}

bool HttpGet::getFile(QUrl url, QString localPath, bool appendToExisting, qint64 startFrom, qint64 bytesCount)
{
    if (downloading && started)
        return false;
    needAbort = false;
    started = false;
//    blockSignals(false);
    errMsg = "";
    errCode = 0;

    if (!fromReGet) //вызвано не из reGetFile?
    {
        rangeSupported = false;

        downloadData.startFrom = startFrom;
        downloadData.bytesCount = bytesCount;
        downloadData.url = url;
        stopOnSize = bytesCount;

        filePath = localPath;
        appentToFile = appendToExisting;

        remoteFileSize = getFileSize(url); //размер файла на сервере. там же установится rangeSupported        
    }

    if (remoteFileSize <= 0)
    {
        return false;
    }

    if (!url.isValid())
    {
        errMsg = trUtf8("Неверный адрес.");
        errCode = QNetworkReply::HostNotFoundError;
        emit netError(static_cast<QNetworkReply::NetworkError>(errCode), errMsg);
        return false;
    }

    QFileInfo fi(filePath);
    if (!fi.absoluteDir().exists())
    {
        errMsg = trUtf8("Каталог не существует");
        emit localError(trUtf8("Каталог не существует"));
        return false;
    }


    file.setFileName(fi.absoluteFilePath());
    QIODevice::OpenMode flags = QIODevice::ReadWrite;    

    //в gotBytes размер файла засунуть, процент посчитать, получить размер на сервере
    gotBytes = file.size();
    //если все получили, то нафиг отсюда    
    if ((gotBytes + startFrom >= remoteFileSize) || (bytesCount && (gotBytes >= bytesCount))
            || (stopOnSize && (gotBytes >= stopOnSize)))
    {
        qDebug() << "Got All. Not starting. " + getFileName();
        gotEverything = true;
        percent = 100;
        emit finished();
        return true;
    }
    else
        gotEverything = false;

    if (appentToFile && rangeSupported)
    {
        flags |= QIODevice::Append;
        if (file.exists())
        {    
            startFrom += gotBytes;
            if (bytesCount)
                bytesCount -= gotBytes;
        }
    }
    else
        flags |= QIODevice::Truncate;


    if (!file.open(flags))
    {
        errMsg = trUtf8("Ошибка открытия локального файла");
        emit localError(trUtf8("Ошибка открытия локального файла"));
        qDebug() << trUtf8("Ошибка открытия локального файла");
        return false;
    }


    if (bytesCount)
    {
        if (startFrom + bytesCount > remoteFileSize)
            bytesCount = remoteFileSize - startFrom;
    }


    lastGotBytes = gotBytes;
    speed = 0;
    lastTime = QTime::currentTime();
    curTime = lastTime;
    calcPercent();
//    percent = (gotBytes * 100) / remoteFileSize;

    QNetworkRequest req(url);

    QString referer = url.toString(QUrl::RemovePath);    
    QString headerValue("bytes="+QString::number(startFrom)+"-"+
                        (bytesCount ? QString::number(startFrom + bytesCount - 1) : ""));
    qDebug() << headerValue;
    if (rangeSupported)
    {
        req.setRawHeader(QByteArray("Range"),
                         headerValue.toStdString().c_str());
    }
    if (!referer.isEmpty())
        req.setRawHeader("Referer", referer.toStdString().c_str());

    reply = manager.get(req);

    connect(reply, SIGNAL(finished()), this, SLOT(downloadFinished()));
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
    connect(reply, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(reply, SIGNAL(metaDataChanged()), this, SLOT(onMetadataChanged()));
    started = true;

    idleIntervalsCounter = 0;
    timer.start();

    return true;
}

bool HttpGet::reGetFile()
{    
    if (remoteFileSize <= 0) //сеть отвалилась раньше запуска секции, размера файла нет, перезапуск не сработает
        fromReGet = false;
    else
        fromReGet = true;
    bool res = getFile(downloadData.url, filePath, appentToFile, downloadData.startFrom, downloadData.bytesCount);
    fromReGet = false;
    return res;
}

bool HttpGet::isDownloading() const
{
    return downloading;
}

bool HttpGet::isAborting() const
{
    return needAbort;
}

void HttpGet::abortDownload()
{
    if (/*started && */reply)
    {
        qDebug() << "Aborting " + getFileName();
        needAbort = true;
        disconnect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
        disconnect(reply, SIGNAL(readyRead()), this, SLOT(readData()));
        disconnect(reply, SIGNAL(metaDataChanged()), this, SLOT(onMetadataChanged()));
        reply->abort();
    }
    else
    {
        qDebug() << "-------NOT ABORTED----" + getFileName();
    }
//    blockSignals(true);
}

/*bool HttpGet::wasGetFileCalled()
{
    return getFileCalled;
}
*/
qint64 HttpGet::getFileSize(QUrl url)
{
    if (!url.isValid())
        return 0;
    QNetworkRequest req(url);
    //isGetSizeActive = true;

    sReply = manager.head(req);
//    connect(sReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    QTimer::singleShot(TIMEOUT, this, SLOT(onGetFileSizeTimeout()));
    while ((!sReply.isNull()) && (sReply->isRunning()))
    {
        QApplication::processEvents();
    }
    if (sReply.isNull())//timeout сработал
        return 0;
    quint64 res = sReply->header(QNetworkRequest::ContentLengthHeader).toULongLong();

    rangeSupported = sReply->hasRawHeader("Accept-Ranges");

    errCode = sReply->error();
    if (errCode == QNetworkReply::NoError)
        errMsg = "";
    else
    {
        res = 0;
        errMsg = sReply->errorString();
        emit netError(sReply->error(), errMsg);
    }
    //sReply->deleteLater();
    delete sReply;
    return res;
}

qint64 HttpGet::getRemoteFileSizeSaved()
{
    return remoteFileSize;
}

qint64 HttpGet::getDownloadingFileSize() const
{
    return remoteFileSize;
}

uchar HttpGet::getPercent() const
{
    return percent;
}

qint64 HttpGet::getBytesReceived() const
{
    return gotBytes;
}

uint HttpGet::getLastErrCode() const
{
    return errCode;
}

QString HttpGet::getLastErrMsg() const
{
    return errMsg;
}

int HttpGet::getSpeed() const
{
    return speed;
}

bool HttpGet::canResume() const
{
    return rangeSupported;
}

DownloadData HttpGet::getDownloadData() const
{
    return downloadData;
}

bool HttpGet::setStopOnSize(qint64 fSize)
{
    /*if (fSize >= gotBytes)
        return false;*/
    stopOnSize = fSize;
    if (gotBytes > stopOnSize)
        reply->abort();
    return true;
}

QString HttpGet::getFilePath() const
{
    return filePath;
}

QString HttpGet::getFileName() const
{
    return QFileInfo(filePath).fileName();
}

bool HttpGet::gotAll() const
{
    return gotEverything;
}

int HttpGet::getLastReplyCode() const
{
    return lastReplyCode;
}

bool HttpGet::hasStarted()
{
    return started;
}

int HttpGet::getIdleIntervals()
{
    return idleIntervalsCounter;// * TIMEOUT_COUNTER_INTERVAL / 1000;
}

void HttpGet::calcPercent()
{
    qint64 dSize;

    if (downloadData.bytesCount)
        dSize = downloadData.bytesCount;
    else
        if (downloadData.startFrom)
            dSize = remoteFileSize - downloadData.startFrom;
        else
            if (stopOnSize)
                dSize = stopOnSize;
            else
                dSize = remoteFileSize;

    percent = (gotBytes * 100) / dSize;
}

void HttpGet::onNeedAbort()
{
    abortDownload();
}

void HttpGet::onGetFileSizeTimeout()
{
    if ((!sReply.isNull()) && (sReply->isRunning()))
    {
        sReply->abort();
        sReply->deleteLater();
        sReply = 0;
    }
}

void HttpGet::onCheckIdleTimer()
{
    idleIntervalsCounter++;
    if (idleIntervalsCounter > IDLE_TIMEOUT_INTERVALS)
    {
        if (speed > 0)
        {
            speed = 0;
            emit downloadFileProgress(gotBytes, remoteFileSize, percent, speed);
        }

    }
    if (idleIntervalsCounter > INTERVALS_TO_ABORT)
    {
        errMsg = trUtf8("Время ожидания истекло.");
        emit netError(QNetworkReply::TimeoutError, errMsg);
        qDebug() << "timeout in " << QFileInfo(filePath).fileName();
        abortDownload();
    }
}


void HttpGet::downloadFinished()
{
    if (stopOnSize)
    {        
        if (file.size() > stopOnSize)
            file.resize(stopOnSize);
    }

    if ((gotBytes + downloadData.startFrom >= remoteFileSize)
            || (downloadData.bytesCount && (gotBytes >= downloadData.bytesCount))
            || (stopOnSize && (gotBytes >= stopOnSize)))
    {
        gotEverything = true;
        percent = 100;
    }
    else
        gotEverything = false;

//    qDebug() << "finished() in HttpGet section " + getFileName();
    file.close();
    disconnect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
    disconnect(reply, SIGNAL(readyRead()), this, SLOT(readData()));
    disconnect(reply, SIGNAL(metaDataChanged()), this, SLOT(onMetadataChanged()));
    idleIntervalsCounter = 0;
    timer.stop();
    reply->deleteLater();
//    reply = 0;
//    needAbort = false;
    downloading = false;
    started = false;
    emit finished();
}

void HttpGet::error(QNetworkReply::NetworkError code)
{
    errCode = code;
    if (reply)
        errMsg = reply->errorString();
    /*
     * если нужен код не QNetworkReply::NetworkError, а HTTP
     *
    QVariant vReplCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if (vReplCode.isValid())
        errCode = vReplCode.toInt();
    emit netError(static_cast<QNetworkReply::NetworkError>(errCode), errMsg);
    */
    qDebug() << errMsg;
    emit netError(code, errMsg);
}

void HttpGet::downloadProgress(qint64 bytesReceived, qint64 totalBytes)
{
    Q_UNUSED(bytesReceived);
    Q_UNUSED(totalBytes);

    qint64 dSize;

    idleIntervalsCounter = 0;

    if (downloadData.bytesCount)
        dSize = downloadData.bytesCount;
    else
        if (downloadData.startFrom)
            dSize = remoteFileSize - downloadData.startFrom;
        else
            if (stopOnSize)
                dSize = stopOnSize;
            else
                dSize = remoteFileSize;


    percent = (gotBytes * 100) / dSize;
    curTime = QTime::currentTime();
    int seconds = abs(lastTime.secsTo(curTime));
    if (seconds >= SECS_FOR_SPEED)
    {
        speed = ((gotBytes - lastGotBytes)  / seconds) / 1024;
        lastGotBytes = gotBytes;
        lastTime = curTime;
    }
    if (!needAbort)
        emit downloadFileProgress(gotBytes, dSize, percent, speed);
}

void HttpGet::readData()
{    
/*    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (statusCode == 416) //Request Range not satisfied (плохой Range)
        return;
        */
    if (!reply)
    {
        qDebug() << "----REPLY DELETED----";
        return;
    }

    if (needAbort)
    {   //хз, вдруг не успеют слоты отключиться и еще раз вызовется onNeedAbort..
//        disconnect(reply, SIGNAL(readyRead()), this, SLOT(readData()));
//        QTimer::singleShot(0, this, SLOT(onNeedAbort()));
        return;
    }

    if (reply->error() == QNetworkReply::NoError)// && reply->isOpen())
    {        
        if (!downloading)
        {
            downloading = true;
            emit downloadStarted(rangeSupported);//reply->hasRawHeader("Accept-Ranges"));
        }

        if (gotEverything)
        {
//             disconnect(reply, SIGNAL(readyRead()), this, SLOT(readData()));
//            QTimer::singleShot(0, this, SLOT(onNeedAbort()));
            return;
        }

        if (needAbort)
            return;
        QByteArray buff = reply->readAll();
        int readBytes = buff.size();
        if (stopOnSize)
        {
            qint64 newGotBytes = gotBytes + readBytes;
            if (newGotBytes > stopOnSize)
            {
                qDebug() << trUtf8("Обрезание! Получено: ") << newGotBytes << trUtf8("Надо: ") << stopOnSize;
                int delCount = newGotBytes - stopOnSize;
                if (delCount > readBytes)
                    buff.clear();
                else
                    buff.remove(readBytes - delCount, delCount);
                file.write(buff);
                gotBytes = stopOnSize;
                gotEverything = true;
                 disconnect(reply, SIGNAL(readyRead()), this, SLOT(readData()));
                QTimer::singleShot(0, this, SLOT(onNeedAbort()));
                return;
            }
        }
        file.write(buff);
        gotBytes += readBytes;
    }
}

void HttpGet::onMetadataChanged()
{
    if (!reply)
        return;
    QVariant varAttr = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if (!varAttr.isValid())
        return;

    lastReplyCode = varAttr.toInt();

    QString statusMsg = "";
    statusMsg = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    emit replyCode(lastReplyCode, statusMsg); //сигналим код ответа
    if (lastReplyCode == 416) //Request Range not satisfied (плохой Range)
    {
        QTimer::singleShot(0, this, SLOT(onNeedAbort()));
        return;
    }

    varAttr = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (varAttr.isValid()) //произошло перенаправление
    {
        needAbort = true; //вдруг попадем в readyRead до остановки
        QString newURL = varAttr.toString();
        downloadData.url = newURL;
        QTimer::singleShot(0, this, SLOT(onNeedAbort()));
        emit redirectToURL(newURL);
    }
}
