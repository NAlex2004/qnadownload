#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "downloadfiledata.h"
#include "downloadnamedelegate.h"
#include "downloadprogressdelegate.h"
#include "downloadtablemodel.h"
#include "downloadsortfiltermodel.h"
#include "downloadtimedelegate.h"
#include "adddialog.h"
#include <QMessageBox>
#include <QUrl>
#include "QFileInfo"
#include <QDialog>
#include <QLabel>
#include <QDialogButtonBox>
#include <QtDebug>
#include <QPushButton>
#include <QSettings>
#include <QClipboard>
#include <QRegExp>
//#include <QRegExpValidator>
#include <functional>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tvSections->setVisible(ui->cbThreads->isChecked());
    QObject::connect(ui->mainToolBar, SIGNAL(visibilityChanged(bool)), this, SLOT(on_toolBarToggleAction_toggled(bool)));

    downloadModel = new DownloadTableModel(this);
    DownloadNameDelegate* nameDelegate = new DownloadNameDelegate(this);
    DownloadProgressDelegate* progressDelegate = new DownloadProgressDelegate(this);
    DownloadTimeDelegate* timeDelegate = new DownloadTimeDelegate(this);

    filterModel = new DownloadSortFilterModel(this);
    filterModel->setSourceModel(downloadModel);

    ui->downloadView->setModel(filterModel);
    ui->downloadView->setItemDelegateForColumn(0, nameDelegate);
    ui->downloadView->setItemDelegateForColumn(3, progressDelegate);
    ui->downloadView->setItemDelegateForColumn(5, timeDelegate);
    ui->downloadView->setItemDelegateForColumn(6, timeDelegate);
    ui->downloadView->resizeColumnsToContents();
    ui->downloadView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->downloadView->setSelectionMode(QAbstractItemView::SingleSelection);

    sectionsModel = new DownloadSectionsModel(0, this);
    ui->tvSections->setModel(sectionsModel);
//    DownloadProgressDelegate* secProgressDelegate = new DownloadProgressDelegate(this);
    ui->tvSections->setItemDelegateForColumn(1, progressDelegate);

    setViewColumnWidth();
    setContextMenus();

    ui->downloadView->setSortingEnabled(true);
    ui->statusBar->setStyleSheet("QStatusBar::item { border: 0px solid black }; ");

    QItemSelectionModel* selModel = ui->downloadView->selectionModel();
//    connect(selModel, SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
//            this, SLOT(downloadView_currentRowChanged(QModelIndex,QModelIndex)));
    connect(selModel, SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            this, SLOT(downloadView_selectionChanged(QItemSelection,QItemSelection)));

    setWindowTitle(TITLE);
    statusLabel.setAlignment(Qt::AlignLeft);

    ui->statusBar->addPermanentWidget(&statusLabel);
    timer.setInterval(TIMER_INTERVAL);
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    active = false;
    restoreSettings();
    createTrayIcon();
    timer.start();

    QStringList args = QApplication::arguments();
    if (args.size() > 1)
    {
        QString param = args.at(1);
        QClipboard* clip = QApplication::clipboard();
        clip->setText(param);
        QTimer::singleShot(0, this, SLOT(on_addAction_triggered()));
    }
}

MainWindow::~MainWindow()
{    
    delete ui;
}

void MainWindow::setViewColumnWidth()
{
    ui->downloadView->setColumnHidden(STATE_COLUMN, true);
    ui->downloadView->setColumnWidth(0, 170);
    ui->downloadView->setColumnWidth(1, 90);
    ui->downloadView->setColumnWidth(2, 90);
    ui->downloadView->setColumnWidth(3, 150);
    ui->downloadView->verticalHeader()->setDefaultSectionSize(ui->downloadView->verticalHeader()->minimumSectionSize());
    ui->tvSections->verticalHeader()->setDefaultSectionSize(ui->tvSections->verticalHeader()->minimumSectionSize());
    ui->tvSections->setColumnWidth(1, 250);
    ui->tvSections->setColumnWidth(3, 200);
}

void MainWindow::setContextMenus()
{
    ui->downloadView->addAction(ui->addAction);
    ui->downloadView->addAction(ui->deleteAction);
    QAction* sep = new QAction(ui->downloadView);
    sep->setSeparator(true);
    QAction* sep2 = new QAction(ui->downloadView);
    sep2->setSeparator(true);
    ui->downloadView->addAction(sep);
    ui->downloadView->addAction(ui->runAction);
    ui->downloadView->addAction(ui->pauseAction);
    ui->downloadView->addAction(ui->editAction);
    ui->downloadView->addAction(sep2);
    ui->downloadView->addAction(ui->restartAction);
}

void MainWindow::connectToModel(QPointer<DownloadFileData> signalSender)
{
    connect(signalSender, SIGNAL(allFinished()), downloadModel, SLOT(rowDataChanged()));
    connect(signalSender, SIGNAL(allFinished()), this, SLOT(onDownloadFinished()));
    connect(signalSender, SIGNAL(downloadProgress(qint64,qint64,uchar,int)), downloadModel, SLOT(rowDataChanged()));
    connect(signalSender, SIGNAL(error(QString)), downloadModel, SLOT(rowDataChanged()));
    connect(signalSender, SIGNAL(started(bool)), downloadModel, SLOT(rowDataChanged()));
    connect(signalSender, SIGNAL(stopped()), downloadModel, SLOT(rowDataChanged()));
    connect(signalSender, SIGNAL(stateChanged(DFState)), downloadModel, SLOT(stateChanged()));
    connect(signalSender, SIGNAL(timeCounterSignal(uint)), downloadModel, SLOT(timeChanged()));

    connect(signalSender, SIGNAL(downloadProgress(qint64,qint64,uchar,int)), this, SLOT(updateSectionDetails()));
    connect(signalSender, SIGNAL(error(QString)), this, SLOT(updateSectionDetails()));
    connect(signalSender, SIGNAL(stopped()), this, SLOT(updateSectionDetails()));
    connect(signalSender, SIGNAL(stateChanged(DFState)), this, SLOT(onStateChanged()));
}

bool MainWindow::confirmDelete(const QString &dialogText, bool &deleteFileAlso)
{
    QDialog* confDialog = new QDialog(this);
    QVBoxLayout* vbLayout = new QVBoxLayout(confDialog);
    QLabel* lab = new QLabel(confDialog);
    QLabel* lIcon = new QLabel(confDialog);
    QDialogButtonBox* bBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, confDialog);
    bBox->button(QDialogButtonBox::Cancel)->setDefault(true);
    connect(bBox, SIGNAL(accepted()), confDialog, SLOT(accept()));
    connect(bBox, SIGNAL(rejected()), confDialog, SLOT(reject()));
    QCheckBox* cbDelFile = new QCheckBox(confDialog);
    cbDelFile->setText(trUtf8("Удалить также с диска"));
    confDialog->setWindowTitle(trUtf8("Удаление"));
//    confDialog->setWindowIcon(QIcon(":/res/delete.png"));
    lab->setText(dialogText);
    lab->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    QFont fnt = lab->font();
    fnt.setBold(true);
    lab->setFont(fnt);
    QPixmap pix(":/res/deleteall.png");
    lIcon->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    lIcon->setPixmap(pix);

    QHBoxLayout* hbLayout = new QHBoxLayout();
    hbLayout->addWidget(lIcon);
    hbLayout->addWidget(lab);
    vbLayout->addLayout(hbLayout);
    vbLayout->addWidget(cbDelFile);
    vbLayout->addWidget(bBox);
//    confDialog->setModal(true);
    vbLayout->setSizeConstraint(QLayout::SetFixedSize);
    bool res = confDialog->exec();
    deleteFileAlso = cbDelFile->isChecked();
    delete confDialog;
    return res;
}

QModelIndex MainWindow::getIndexByProxy(const QModelIndex &proxyIndex) const
{
    if (proxyIndex.isValid())
        return filterModel->mapToSource(proxyIndex);
    else
        return QModelIndex();
}

QPointer<DownloadFileData> MainWindow::getFileDataByProxy(const QModelIndex &proxyIndex) const
{
    if (proxyIndex.isValid())
        return downloadModel->getRowData(getIndexByProxy(proxyIndex).row());
    else
        return 0;//nullptr;
}

void MainWindow::saveSettings()
{
    QSettings sett(QSettings::NativeFormat, QSettings::UserScope, ORGNAME, APPNAME);
    int selRow;
    QModelIndex ind;

    sett.beginGroup("Window");
    sett.setValue("Pos", pos());
    sett.setValue("Size", size());    
    sett.setValue("Details", ui->cbThreads->isChecked());
    sett.setValue("Maximized", isMaximized());
    sett.setValue("SaveDir", saveDir);
    if (!ui->downloadView->selectionModel()->selection().isEmpty())
    {
        ind = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        selRow = ind.row();
    }
    else
        selRow = -1;
    sett.setValue("Selected", selRow);
    sett.endGroup();

    sett.beginGroup("Downloads");
    sett.remove("");

    QString grName;
    int rc = downloadModel->rowCount();
    QPointer<DownloadFileData> downloader;
    for(int i=0; i<rc; i++)
    {
        downloader = downloadModel->getRowData(i);
        grName = QString::number(i);
        sett.setValue(grName + "/URL", downloader->getURL());
        sett.setValue(grName + "/Path", downloader->getLocalPath());
        sett.setValue(grName + "/Threads", downloader->getThreadCount());
        sett.setValue(grName + "/Percent", downloader->getPercent());
        sett.setValue(grName + "/File Size", downloader->getFileSize());
        sett.setValue(grName + "/Got Bytes", downloader->getBytesDone());
        sett.setValue(grName + "/State", static_cast<int>(downloader->getState()));
        sett.setValue(grName + "/Time Elapsed", downloader->getTimeElapsed());
    }

    sett.endGroup();
}

void MainWindow::restoreSettings()
{
    QSettings sett(QSettings::NativeFormat, QSettings::UserScope, ORGNAME, APPNAME);

    sett.beginGroup("Window");
    bool wMax = sett.value("Maximized", false).toBool();
    if (wMax)
        setWindowState(windowState() | Qt::WindowMaximized);
    else
    {
        QSize wSize = sett.value("Size", QSize(0, 0)).toSize();
        if (!wSize.isEmpty())
            resize(wSize);
        QPoint wPos = sett.value("Pos", QPoint(0, 0)).toPoint();
        if (!wPos.isNull())
            move(wPos);
    }
    ui->cbThreads->setChecked(sett.value("Details", false).toBool());
    int selRow = sett.value("Selected", -1).toInt();
    saveDir = sett.value("SaveDir", "").toString();
    sett.endGroup();

    sett.beginGroup("Downloads");

    QStringList groups = sett.childGroups();
    int rc = groups.size();
    QPointer<DownloadFileData> downloader;
    QString url;
    QString path;
    uchar threads;
    uchar percent;
    qint64 fSize;
    qint64 bDone;
    DFState dState;
    uint timeElapsed;

    for(int i=0; i<rc; i++)
    {
        sett.beginGroup(groups.at(i));

        path = sett.value("Path", "").toString();
        if (!path.isEmpty())
        {
            url = sett.value("URL", "").toString();
            if (!url.isEmpty())
            {
                threads = sett.value("Threads", DEF_THREAD_COUNT).toUInt();
                if (threads == 0)
                    threads = DEF_THREAD_COUNT;
                percent = sett.value("Percent", 0).toUInt();
                fSize = sett.value("File Size", 0).toLongLong();
                bDone = sett.value("Got Bytes", 0).toLongLong();
                dState = static_cast<DFState>(sett.value("State", 0).toInt());
                timeElapsed = sett.value("Time Elapsed", 0).toUInt();

                downloader = new DownloadFileData(url, path, threads, this);
                downloader->setPercent(percent);
                downloader->setRemoteFileSize(fSize);
                downloader->setTimeElapsed(timeElapsed);
                downloader->setBytesDone(bDone);

                connectToModel(downloader);
                downloadModel->appendRow(downloader);
                if (dState == dfDownloading)
                {
                    downloader->start();
                }
                else
                {
                    downloader->setState(dState);
                }
            }
        }

        sett.endGroup();
    }
    sett.endGroup();

    if ((rc > 0) && (selRow >= 0))
    {
//        if (downloadModel->rowCount() > 0)
            ui->downloadView->setFocus(Qt::MouseFocusReason);
            ui->downloadView->selectRow(selRow);
            QModelIndex ind = filterModel->mapToSource(ui->downloadView->selectionModel()->selectedRows(0).at(0));
            refreshActionsByIndex(ind);
    }
    else
        refreshActionsByIndex(QModelIndex());   
}

void MainWindow::createTrayIcon()
{
    QIcon icon(":/res/qNADownload-inactive-16.png");
    trayIcon = new QSystemTrayIcon(icon, this);
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(onTrayIconActivated(QSystemTrayIcon::ActivationReason)));
    QAction* actRestore = new QAction(trUtf8("Показать"), this);
    QAction* actQuit = new QAction(trUtf8("Выход"), this);
    QMenu* trayMenu = new QMenu(this);
    connect(actRestore, SIGNAL(triggered(bool)), this, SLOT(showAsItWas()));
    connect(actQuit, SIGNAL(triggered(bool)), this, SLOT(close()));
    trayMenu->addAction(actRestore);
    trayMenu->addAction(actQuit);
    trayIcon->setContextMenu(trayMenu);
    trayIcon->show();
}

bool MainWindow::fileOrDownloadExists(QString path, QModelIndex currIndex)
{
    QFileInfo fi(path);
    if (fi.exists())
        return true;

    int rc = downloadModel->rowCount();
    QString lcPath = path.toLower();
    QPointer<DownloadFileData> downloader;
    int currRow = -1;
    if (currIndex.isValid())
        currRow = currIndex.row();

    for(int i=0; i<rc; i++)
    {
        if (i != currRow)
        {
            downloader = downloadModel->getRowData(i);
            if (!downloader.isNull())
            {
                if (lcPath.compare(downloader->getLocalPath().toLower()) == 0)
                    return true;
            }
        }
    }
    return false;
}

QString MainWindow::getURLFromClipboard()
{
    QClipboard* cpb = QApplication::clipboard();
    QString cpbText = cpb->text();
    if (cpbText.isEmpty())
        return QString();
    QRegExp rExp("^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*");
//    QRegExpValidator rVal(rExp);
//    int i;
//    if (rVal.validate(cpbText, i) == QRegExpValidator::Acceptable)

    if (rExp.exactMatch(cpbText))
        return cpbText;
    else
        return QString();

}

void MainWindow::on_filterList_currentRowChanged(int currentRow)
{    
    switch (currentRow)
    {
    case 1:
        filterModel->setFilterSet(DFStateSet() << dfDownloading);
        break;
    case 2:
        filterModel->setFilterSet(DFStateSet() << dfDone);
        break;
    case 3:
        filterModel->setFilterSet(DFStateSet() << dfPaused);
        break;
    case 4:
        filterModel->setFilterSet(DFStateSet() << dfError);
        break;
    default:
        filterModel->setFilterSet(DFStateSet() << dfUnknown << dfDownloading << dfDone << dfError << dfPaused);
    }
}

void MainWindow::on_toolBarToggleAction_toggled(bool arg1)
{
    if (ui->mainToolBar->isVisible() != arg1)
    {
        ui->mainToolBar->setVisible(arg1);
    }
    ui->toolBarToggleAction->setChecked(arg1);
}

void MainWindow::downloadView_currentRowChanged(const QModelIndex &current, const QModelIndex &previous)
{
    Q_UNUSED(previous);
    if (!current.isValid())
    {
        sectionsModel->setSectionList(0);
        refreshActionsByIndex(QModelIndex());
        return;
    }
    if (!ui->downloadView->selectionModel()->selection().isEmpty())
    {
        QModelIndex ind = filterModel->mapToSource(current);
        refreshActionsByIndex(ind);
        DownloadFileData* fData = downloadModel->getRowData(ind.row());//getFileDataByProxy(current);
        sectionsModel->setSectionList(fData->getSectionList());
    }



    //setWindowTitle(QString::number(current.row()));
}

void MainWindow::downloadView_selectionChanged(QItemSelection selected, QItemSelection deselected)
{
    Q_UNUSED(deselected);

    if (selected.isEmpty())
    {
        sectionsModel->setSectionList(0);
        refreshActionsByIndex(QModelIndex());
    }
    else
    {
        QModelIndex ind = getIndexByProxy(selected.indexes().at(0));
        refreshActionsByIndex(ind);
        DownloadFileData* fData = downloadModel->getRowData(ind.row());//getFileDataByProxy(current);
        sectionsModel->setSectionList(fData->getSectionList());
    }

}

void MainWindow::on_aboutAction_triggered()
{
    QMessageBox::about(this, trUtf8("О программе qNADownload"), ABOUT);
}

void MainWindow::on_aboutQtAction_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_deleteAction_triggered()
{
    if (!ui->downloadView->selectionModel()->selectedRows().isEmpty())
    {
        QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        QModelIndex ind = filterModel->mapToSource(proxyInd);        
        int row = ind.row();
        int proxyRow = proxyInd.row();

        if (ind.isValid())
        {
            DownloadFileData* fData = downloadModel->getRowData(row);
            bool delFromDisk;
            bool res = confirmDelete(trUtf8("Удалить закачку \n") +fData->getFileName() + " ?", delFromDisk);
//                    QMessageBox::question(this, trUtf8("Удаление закачки"), trUtf8("Удалить закачку \n") +
//                                            fData->getFileName() + " ?");

            if (res)
            {
                fData->stop();
                //if (delFromDisk)
                    fData->setDeleteOnDestroy(true, delFromDisk);
                    //fData->deleteLocalFile();
                downloadModel->removeRow(row);
                int rc = downloadModel->rowCount();
                if (rc > 0)
                {
                    proxyRow = qBound(0, proxyRow, rc-1);
                    ui->downloadView->selectRow(proxyRow);
                    proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
                    ind = filterModel->mapToSource(proxyInd);
                    refreshActionsByIndex(ind);
                }
                else
                    refreshActionsByIndex(QModelIndex());
            }
        }
    }
}

void MainWindow::on_addAction_triggered()
{
    CheckDelegate checkExists;
    checkExists = std::bind(&MainWindow::fileOrDownloadExists, this, std::placeholders::_1, std::placeholders::_2);
    AddDialog addDownload(this, checkExists);

    QString cpbText = getURLFromClipboard();
    if (!cpbText.isEmpty())
        addDownload.setURL(cpbText);

    if (addDownload.exec(saveDir))
    {
        QUrl URL;
        QString path;
        uchar threads;
        bool runImmediate;
        addDownload.getResults(URL, path, threads, runImmediate);
        if (!path.isEmpty())
           saveDir = QFileInfo(path).path();

        //QMessageBox::information(this, "", URL.toString() + "\n" + path);
        DownloadFileData* fData = new DownloadFileData(URL.toString(), path, threads);
//        fData->setMaxThreads(threads);
        downloadModel->appendRow(fData);
        connectToModel(fData);
        if (runImmediate)
            fData->start();
    }    
}

void MainWindow::on_deleteAllAction_triggered()
{
    if (downloadModel->rowCount() <= 0)
        return;
    bool delFromDisk;
    bool res = confirmDelete(trUtf8("Удалить ВСЕ неактивные закачки?"), delFromDisk);
            /*QMessageBox::warning(this, trUtf8("Удаление закачек."), trUtf8("Удалить ВСЕ закачки?"),
                                   QMessageBox::Yes | QMessageBox::No, QMessageBox::No);*/

    if (res)
    {        
        sectionsModel->setSectionList(0);

//        int rc = downloadModel->rowCount();
        int i = 0;

        while (i < downloadModel->rowCount())
        {
            QPointer<DownloadFileData> fData = downloadModel->getRowData(i);
            if (fData && (fData->getState() != dfDownloading))
            {
//                fData->stop();
                fData->setDeleteOnDestroy(true, delFromDisk);
                downloadModel->removeRow(i);
            }
            else
                i++;
        }

//        downloadModel->removeRows(0, downloadModel->rowCount());
        if (downloadModel->rowCount() == 0)
            refreshActionsByIndex(QModelIndex());
        else
        {
            ui->downloadView->selectRow(0);
//            QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
//            QModelIndex ind = filterModel->mapToSource(proxyInd);
            QModelIndex ind = downloadModel->index(0, 0);
            refreshActionsByIndex(ind);
            sectionsModel->setSectionList(downloadModel->getRowData(0)->getSectionList());
        }
    }
}

void MainWindow::on_editAction_triggered()
{
    if (!ui->downloadView->selectionModel()->selectedRows().isEmpty())
    {
        QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        QModelIndex ind = filterModel->mapToSource(proxyInd);

        if (ind.isValid())
        {
            CheckDelegate checkExists;
            checkExists = std::bind(&MainWindow::fileOrDownloadExists, this, std::placeholders::_1, std::placeholders::_2);
            AddDialog addDownload(this, checkExists, ind);

            addDownload.setWindowTitle(trUtf8("Изменить закачку"));
            DownloadFileData* efData = downloadModel->getRowData(ind.row());
            if (addDownload.exec(efData->getURL(), efData->getLocalPath(), efData->getMaxThreads()))
            {
                QUrl URL;
                QString path;
                uchar threads;
                bool runImmediate;
                addDownload.getResults(URL, path, threads, runImmediate);
                if (!path.isEmpty())
                   saveDir = QFileInfo(path).path();

                if ((URL == efData->getURL()) && (path == efData->getLocalPath()) && (threads == efData->getMaxThreads()))
                    return; //ничего не изменилось
                efData->setURL(URL.toString());
                efData->setLocalPath(path);
                efData->setMaxThreads(threads);


                //DownloadFileData* fData = new DownloadFileData(URL.toString(), path);
                downloadModel->setRowData(ind.row(), efData);//если что, туда удаление файлов всунуть
                sectionsModel->setSectionList(efData->getSectionList());
                if (runImmediate)
                    efData->restart();
            }
        }
    }
}

void MainWindow::on_downloadView_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);
    if (ui->editAction->isEnabled())
            on_editAction_triggered();
}

void MainWindow::on_runAction_triggered()
{
    if (!ui->downloadView->selectionModel()->selectedRows().isEmpty())
    {
        QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        QModelIndex ind = filterModel->mapToSource(proxyInd);

        if (ind.isValid())
        {
            DownloadFileData* fData = downloadModel->getRowData(ind.row());
            fData->resume();
            refreshActionsByIndex(ind);
        }
    }
}

void MainWindow::on_pauseAction_triggered()
{
    if (!ui->downloadView->selectionModel()->selectedRows().isEmpty())
    {
        QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        QModelIndex ind = filterModel->mapToSource(proxyInd);

        if (ind.isValid())
        {
            DownloadFileData* fData = downloadModel->getRowData(ind.row());
            fData->stop();
        }
    }
}

void MainWindow::on_restartAction_triggered()
{
    if (!ui->downloadView->selectionModel()->selectedRows().isEmpty())
    {
        QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        QModelIndex ind = filterModel->mapToSource(proxyInd);

        if (ind.isValid())
        {
            int res = QMessageBox::question(this, trUtf8("Перезапуск закачки."),
                                            trUtf8("Закачать заново? \n (Старые файлы удаляться.)"),
                                           QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

            if (res == QMessageBox::Yes)
            {
                DownloadFileData* fData = downloadModel->getRowData(ind.row());
                fData->deleteDownload(true);
                fData->restart();
            }
        }
    }
}

void MainWindow::on_stopAllAction_triggered()
{
    if (downloadModel->rowCount() <= 0)
        return;

    int res = QMessageBox::question(this, trUtf8("Остановка."),
                                    trUtf8("Остановить все закачки?"),
                                    QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if (res == QMessageBox::Yes)
    {
        DownloadFileData* fData;// = downloadModel->getRowData(ind.row());
        for(int i=0; i<downloadModel->rowCount(); i++)
        {
            fData = downloadModel->getRowData(i);
            fData->stop();
        }
    }

}

void MainWindow::updateSectionDetails()
{
    sectionsModel->refresh();
}

void MainWindow::onStateChanged()
{
    if (!ui->downloadView->selectionModel()->selection().isEmpty())
    {
        QModelIndex proxyInd = ui->downloadView->selectionModel()->selectedRows(0).at(0);
        QModelIndex ind = filterModel->mapToSource(proxyInd);

        refreshActionsByIndex(ind);
    }
}

void MainWindow::onTimer()
{
    int rc = downloadModel->rowCount();
    QString msg = trUtf8("Нет закачек.");
    QString speedMsg;
    int nActive = 0;

    if (rc > 0)
    {
        int nSpeed = 0;

        QPointer<DownloadFileData> downloader;

        for(int i=0; i<rc; i++)
        {
            downloader = downloadModel->getRowData(i);
            if ((downloader) && (downloader->getState() == dfDownloading))
            {
                    nSpeed += downloader->getSpeed();
                    nActive++;
            }
        }

        msg = trUtf8(QString("Активно: %1 из %2.").arg(nActive).arg(rc).toStdString().c_str());
        if (nActive > 0)
            speedMsg = trUtf8(QString("Скорость %1 Кб/с").arg(nSpeed).toStdString().c_str());
    }

    bool lastActive = active;
    active = (nActive > 0);
    if (active != lastActive)
        trayIcon->setIcon(active ? QIcon(":/res/qNADownload-16.png") : QIcon(":/res/qNADownload-inactive-16.png"));

    statusLabel.setText(msg + "  " + speedMsg);
    trayIcon->setToolTip(msg + (speedMsg.isEmpty() ? "" : "\n" + speedMsg));
}

void MainWindow::refreshActionsByIndex(const QModelIndex &ind)
{
    if (ind.isValid())
    {
        DFState state = static_cast<DFState>(ind.model()->index(ind.row(), STATE_COLUMN).data().toUInt());
        ui->deleteAction->setEnabled(state != dfDownloading); //или просто true, там остановка есть
        ui->runAction->setEnabled(!((state == dfDownloading) || (state == dfDone)));
        ui->pauseAction->setEnabled(state == dfDownloading);
        ui->restartAction->setEnabled(state != dfDownloading);
        ui->editAction->setEnabled(!((state == dfDownloading) || (state == dfDone)));
    }
    else
    {
        ui->deleteAction->setEnabled(false);
        ui->runAction->setEnabled(false);
        ui->pauseAction->setEnabled(false);
        ui->restartAction->setEnabled(false);
        ui->editAction->setEnabled(false);
        sectionsModel->setSectionList(0);
    }
}


void MainWindow::on_runAllAction_triggered()
{
    if (downloadModel->rowCount() <= 0)
        return;

    int res = QMessageBox::question(this, trUtf8("Запуск."),
                                    trUtf8("Запустить все неактивные не законченные закачки?"),
                                    QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if (res == QMessageBox::Yes)
    {
        DownloadFileData* fData;// = downloadModel->getRowData(ind.row());
        for(int i=0; i<downloadModel->rowCount(); i++)
        {
            fData = downloadModel->getRowData(i);
            DFState dState = fData->getState();
            if ((dState != dfDownloading) && (dState != dfDone))
                fData->resume();
        }
    }

}

void MainWindow::onTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason != QSystemTrayIcon::Trigger)
        return;
    if (isVisible())
    {
        hide();
    }
    else
    {
        showAsItWas();
        /*show();
        setWindowState(windowState() ^ (windowState() & Qt::WindowMinimized));
        activateWindow();*/
//        raise();
//        setFocus();
    }
}

void MainWindow::showAsItWas()
{
    show();
    setWindowState(windowState() ^ (windowState() & Qt::WindowMinimized));
    activateWindow();
}

void MainWindow::onDownloadFinished()
{
    DownloadFileData* fData = qobject_cast<DownloadFileData*>(sender());
    QString msg = trUtf8("Закачка ") + fData->getFileName() + trUtf8(" завершена.");
    trayIcon->showMessage(trUtf8("Завершено"), msg, QSystemTrayIcon::Information, 3000);
    updateSectionDetails();
}


void MainWindow::changeEvent(QEvent *evt)
{
    if (evt->type() == QEvent::WindowStateChange)
    {
        if (windowState() & Qt::WindowMinimized)
        {
            hide();
            //если что, то
            //QTimer::singleShot(0, this, SLOT(hide()));
        }
       /* else
        {
            if (windowState() & Qt::WindowMaximized)
                wasMaximized = true;
            else
                wasMaximized = false;
        }*/
    }

    QMainWindow::changeEvent(evt);
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    timer.stop();
    saveSettings();
    event->accept();
}

