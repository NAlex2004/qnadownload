#include "downloadsectionsmodel.h"

DownloadSectionsModel::DownloadSectionsModel(const SectionList * secList, QObject *parent) :
    QAbstractTableModel(parent)
{
    parts = secList;
}

void DownloadSectionsModel::setSectionList(const SectionList *secList)
{
    beginResetModel();
    parts = secList;
    endResetModel();
 //   emit dataChanged(index(0, 0), index(rowCount() - 1, COLCOUNT - 1));
}

void DownloadSectionsModel::refresh()
{
//    qDebug() << rowCount();
//    emit dataChanged(index(0, 0), index(rowCount() - 1, COLCOUNT - 1));
    emit layoutChanged();
//    beginResetModel();
//    endResetModel();
}



int DownloadSectionsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    if (parts)
        return parts->size();

    return 0;
}

int DownloadSectionsModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return COLCOUNT;
}

QVariant DownloadSectionsModel::data(const QModelIndex &index, int role) const
{
    if ((!index.isValid()) || (!parts) || (index.row() < 0) || (index.row() >= parts->size()))
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        int row = index.row();

        switch (index.column())
        {
        case 0:
            return row + 1;
        case 1:
            if (parts->at(row))
                return parts->at(row)->getPercent();
        case 2:
            if (parts->at(row))
                return QString(trUtf8("%1 Кб/с")).arg(parts->at(row)->getSpeed());
        case 3:
            if (parts->at(row))
                return parts->at(row)->getLastErrMsg();
        }
    }

    if ((role == Qt::TextAlignmentRole) && (index.column() == 2))
        return QVariant(Qt::AlignCenter | Qt::AlignVCenter);

    return QVariant();
}

QVariant DownloadSectionsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal && section >= 0 && section < COLCOUNT)
    {
        switch (section)
        {
        case 0:
            return trUtf8("Секция");
        case 1:
            return trUtf8("Прогресс");
        case 2:
            return trUtf8("Скорость");
        case 3:
            return trUtf8("Ошибка");
        }
    }

    return QVariant();
}


Qt::ItemFlags DownloadSectionsModel::flags(const QModelIndex &index) const
{
    if ((!index.isValid()) || (!parts) || (index.row() < 0) || (index.row() >= parts->size()))
        return Qt::NoItemFlags;
    return Qt::ItemIsEnabled;
}
