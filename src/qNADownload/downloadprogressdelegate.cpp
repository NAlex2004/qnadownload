#include "downloadprogressdelegate.h"
#include <QStyleOptionProgressBar>
#include <QApplication>
#include <QPainter>

DownloadProgressDelegate::DownloadProgressDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}


void DownloadProgressDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (!index.isValid())
        return;
    QStyleOptionProgressBar pBar;
    uint value = index.data().toUInt();
    pBar.minimum = 0;
    pBar.maximum = 100;
    pBar.progress = value;
    pBar.rect = option.rect;
    pBar.text = QString::number(value)+"%";
    pBar.textVisible = true;
    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &pBar, painter);
}

QSize DownloadProgressDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    //return QSize(option.rect.width(), option.rect.height());
    return QSize(100, option.rect.height());
}
