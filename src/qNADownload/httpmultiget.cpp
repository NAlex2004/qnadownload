#include "httpmultiget.h"
#include <QFileInfo>
#include <QTimerEvent>
#include <QDir>
#include <limits>
//#include <QFile>
#include <QSet>
#include <QApplication>
#include <QtDebug>

const QSet<QNetworkReply::NetworkError> HttpMultiGet::criticalErrorSet =
        (QSet<QNetworkReply::NetworkError>() << QNetworkReply::ConnectionRefusedError
         << QNetworkReply::HostNotFoundError
         << QNetworkReply::ContentAccessDenied
         << QNetworkReply::ContentOperationNotPermittedError
         << QNetworkReply::AuthenticationRequiredError
         << QNetworkReply::ProtocolUnknownError
         #if QT_VERSION >= 0x050000
         << QNetworkReply::BackgroundRequestNotAllowedError
         << QNetworkReply::ContentGoneError
         << QNetworkReply::ServiceUnavailableError
         << QNetworkReply::OperationNotImplementedError
         #endif
                                                     );


const QString HttpMultiGet::strStates[5] = {QObject::trUtf8("неизвестно"), QObject::trUtf8("скачивается"),
                                         QObject::trUtf8("завершена"), QObject::trUtf8("ошибка"),
                                         QObject::trUtf8("остановлена")};

HttpMultiGet::HttpMultiGet(QString URLPath, QString localFile, uchar threads, QObject *parent):
    QObject(parent),
    state(dfUnknown), localPath(localFile), URL(URLPath), percent(0), fileSize(0),
    /*timeLeft(0, 0),*/ bytesDone(0), threadCount(0), errThreadCount(0),
    finishedThreadCount(0), speed(0), timeElapsed(0), lastTimePr(0, 0), curTimePr(0, 0),
    starting(false), deleteFileOnDestroy(false), deletePartsOnDestroy(false)
{
    //localPath проверить на существование
    connect(&timeCounter, SIGNAL(timeout()), this, SLOT(onTimeCounter()));
    connect(&abortTimer, SIGNAL(timeout()), this, SLOT(onAbortTimer()));
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    timeCounter.setInterval(1000);
    timer.setInterval(INTERVAL);
    maxThreads = qBound((uchar)1, threads, MAX_THREADS);
    threadCount = maxThreads;
    abortTimer.setInterval(200);
}

HttpMultiGet::~HttpMultiGet()
{
    abortTimer.stop();
    if (deletePartsOnDestroy)
        deleteDownload(deleteFileOnDestroy);
    else
        clearParts();
}

QString HttpMultiGet::getLocalPath() const
{
    return localPath;
}

bool HttpMultiGet::setLocalPath(const QString &value)
{
    if (state == dfDownloading)
        return false;
    if (localPath.compare(value) == 0)
        return false;
    if (!QFileInfo(value).dir().exists())
        return false;
    if (!localPath.isEmpty())
        deleteFileParts(); //удалим старые файлы

    localPath = value;
    return true;
}

QString HttpMultiGet::getFileName() const
{
    QFileInfo FInfo(localPath);
    //if (FInfo.exists())
    {
      return FInfo.fileName();
    }
    //return QString(QObject::trUtf8("нет файла"));
}

uchar HttpMultiGet::getPercent() const
{
    return percent;
}

qint64 HttpMultiGet::getFileSize() const
{
    return fileSize;
}

qint64 HttpMultiGet::getBytesDone() const
{
    return bytesDone;
}

uchar HttpMultiGet::getThreadCount() const
{
    return threadCount;
}

uint HttpMultiGet::getTimeElapsed() const
{
    return timeElapsed;
}

uint HttpMultiGet::getTimeLeft() const
{
    if (state == dfDone)
        return 0;
    if (percent > 0 && state == dfDownloading)
        return timeElapsed * 100 / percent - timeElapsed;
    else return std::numeric_limits<uint>::max();
}

DFState HttpMultiGet::getState() const
{
    return state;
}

QString HttpMultiGet::getStateStr() const
{
    return strStates[state];
}

QString HttpMultiGet::getTimeElapsedStr() const
{
    uint secs = timeElapsed;

    uint days = secs / 86400;
    secs -= days * 86400;
    uint hours = secs / 3600;
    secs -= hours * 3600;
    uint minutes = secs / 60;
    secs -= minutes * 60;

    QString text("");

    if (days)
        text.append(QString(trUtf8("%1 д ")).arg(days));
    if (hours)
    {
        if (hours < 10)
            text.append("0");
        text.append(QString("%1:").arg(hours));
    }
    if (minutes < 10)
        text.append("0");
    text.append(QString("%1:").arg(minutes));
    if (secs < 10)
        text.append("0");
    text.append(QString("%1").arg(secs));

    return text;
}

void HttpMultiGet::setPercent(uchar value)
{
    percent = value;
}

void HttpMultiGet::setState(DFState value)
{
    state = value;
    emit stateChanged(state);
}

void HttpMultiGet::setTimeElapsed(uint value)
{
    timeElapsed = value;
}

void HttpMultiGet::setBytesDone(qint64 value)
{
    bytesDone = value;
}

void HttpMultiGet::setRemoteFileSize(qint64 value)
{
    if (state != dfDownloading)
        fileSize = value;
}

int HttpMultiGet::getSpeed() const
{
    return speed;
}

const SectionList * HttpMultiGet::getSectionList()
{
    return &parts;
}

QString HttpMultiGet::getURL() const
{
    return URL;
}

bool HttpMultiGet::setURL(const QString &value)
{
    if (state == dfDownloading)
        return false;
    if (URL.compare(value) == 0)
        return false;
    if ((!URL.isEmpty()) && (!localPath.isEmpty()))
        deleteFileParts();//т.к. практически новая закачка, старые удалим
    URL = value;    
    return true;
}

void HttpMultiGet::setMaxThreads(uchar threads)
{
    if ((threads == maxThreads) || state == dfDownloading)
        return;
    deleteFileParts();//изменили кол-во потоков-> разные части будут
    maxThreads = qBound((uchar)1, threads, MAX_THREADS);
}

uchar HttpMultiGet::getMaxThreads()
{
    return maxThreads;
}

bool HttpMultiGet::start(bool dontGetNewFileSize)
{
    if (state == dfDownloading)
        return false;

    if (URL.isEmpty() || localPath.isEmpty())
    {
        setState(dfError);
        return false;
    }


//    timeElapsed = 0;
    state = dfUnknown;

    errThreadCount = 0;
    finishedThreadCount = 0;    
    starting = true;

    QPointer<HttpGet> downloader = new HttpGet(this); //если что, без this

    qint64 oldFileSize = fileSize;
    if (!dontGetNewFileSize) //это если не из resume пришли, проверив там размеры
        fileSize = downloader->getFileSize(URL);

    if (fileSize == 0)
    {
        setState(dfError);
        emit error(downloader->getLastErrMsg());
        delete downloader;
        starting = false;
        clearParts();
        return false;
    }
    //изменился размер файла, перекачиваем
    if ((oldFileSize != 0) && (oldFileSize != fileSize))
    {
        qDebug() << trUtf8("Размеры не совпадают, удаляем старые секции.");
        deleteFileParts();
    }
    else
        clearParts();

    setState(dfDownloading);

    bool isRangeSupported = downloader->canResume();
    QFileInfo fi(localPath);
    QString hiddenPath;
    hiddenPath = QDir::toNativeSeparators(fi.path() + QDir::separator() + "." + fi.fileName());

    if ((!isRangeSupported) || maxThreads < 2 || fileSize < MIN_SIZE_FOR_MULTI)
    {
        threadCount = 1;
        connect(downloader, SIGNAL(finished()), this, SLOT(onFinished()));
        connect(downloader, SIGNAL(downloadFileProgress(qint64,qint64,uchar,int)),
                this, SLOT(onDownloadProgress(qint64,qint64,uchar,int)));
        connect(downloader, SIGNAL(localError(QString)), this, SLOT(onLocalError(QString)));
        connect(downloader, SIGNAL(netError(QNetworkReply::NetworkError,QString)),
                this, SLOT(onNetworkError(QNetworkReply::NetworkError,QString)));
        connect(downloader, SIGNAL(redirectToURL(QString)), this, SLOT(onRedirectToURL(QString)));
//        connect(downloader, SIGNAL(replyCode(int,QString)), this, SLOT(onReplyCode(int,QString)));            

        downloader->getFile(URL, hiddenPath + ".part0");
        parts.append(downloader);
    }
    else
    {
        //запускаем все секции

        threadCount = maxThreads;

        qint64 partSize = fileSize / maxThreads;
        qint64 lastPartSize = fileSize - partSize * (maxThreads - 1);


        for (int i=0; i<maxThreads; i++)
        {
            if (i != 0)
                downloader = new HttpGet(this); //если что, без this
            connect(downloader, SIGNAL(finished()), this, SLOT(onFinished()));
            connect(downloader, SIGNAL(downloadFileProgress(qint64,qint64,uchar,int)),
                    this, SLOT(onDownloadProgress(qint64,qint64,uchar,int)));
            connect(downloader, SIGNAL(localError(QString)), this, SLOT(onLocalError(QString)));
            connect(downloader, SIGNAL(netError(QNetworkReply::NetworkError,QString)),
                    this, SLOT(onNetworkError(QNetworkReply::NetworkError,QString)));
            connect(downloader, SIGNAL(redirectToURL(QString)), this, SLOT(onRedirectToURL(QString)));
    //        connect(downloader, SIGNAL(replyCode(int,QString)), this, SLOT(onReplyCode(int,QString)));

            downloader->getFile(URL, hiddenPath + ".part" + QString::number(i), true,
                                       i * partSize, (i < (maxThreads - 1) ? partSize : lastPartSize));
            parts.append(downloader);
        }

    }


//    if (state == dfDownloading)
    timer.start();
    timeCounter.start();
    emit started(isRangeSupported);
    starting = false;
    return true;
}

bool HttpMultiGet::restart()
{
    timeElapsed = 0;
    return start();
}

void HttpMultiGet::stop()
{
    if ((state != dfDownloading) || (abortTimer.isActive()))
        return;

//    setState(dfPaused);
    timer.stop();
    abortTimer.start();
}


bool HttpMultiGet::resume()
{
    /* проверка существования файла - дело вызывающего
    QFileInfo fi(localPath);
    if (fi.exists() && fi.size() == fileSize)
    {
        emit allFinished();
        return true;
    }
    */

    if (parts.isEmpty())
        return start();
    QPointer<HttpGet> part = parts.at(0);
    if (part)
    {
        qint64 oldFileSize = fileSize;
        fileSize = part->getFileSize(URL);

        if (fileSize == 0)
        {
            fileSize = oldFileSize;
            setState(dfError);
            QString errMsg = part->getLastErrMsg();
            if (errMsg.isEmpty())
                errMsg = trUtf8("Файл не существует, или ошибка сети");
            emit error(errMsg);
            starting = false;
            return false;
        }

        if (fileSize != oldFileSize)
        {
            qDebug() << "filesize not same";
            deleteFileParts();
            return start(true);
        }
    }
    else
    {
        //вряд ли, но вдруг убилась секция
        qDebug() << trUtf8("Секции убились что-ли.. Стартуем заново");
        return start();
    }
    if (state == dfDone)
        timeElapsed = 0;
    bool res = false;
    bool canResume = false;
    int doneCount = 0;
    errThreadCount = 0;
    finishedThreadCount = 0;
    starting = true;

    for (QList<QPointer<HttpGet> >::iterator downloader = parts.begin(); downloader != parts.end(); downloader++)
    {
        res |= (*downloader)->reGetFile();
        if (res)
            canResume |= (*downloader)->canResume();
        if ((*downloader)->gotAll())
            doneCount++;
    }

    if (res)
    {
        state = dfDownloading;
        startTimers();
        emit started(canResume);
    }
    else
        if (doneCount == parts.size())
        {
            setState(dfDone);
        }
        else
            setState(dfError);

    starting = false;
    return res;
}

void HttpMultiGet::deleteDownload(bool deleteFile)
{
    deleteFileParts();
    if (deleteFile)
    {
        deleteLocalFile();
    }
}

void HttpMultiGet::deleteLocalFile()
{
    QFileInfo fi(localPath);
    if (fi.exists())
    {
        QFile file(fi.absoluteFilePath());
        file.remove();
    }
}

void HttpMultiGet::setDeleteOnDestroy(const bool delParts, const bool delLocalFile)
{
    deleteFileOnDestroy = delLocalFile;
    deletePartsOnDestroy = delParts;
}


void HttpMultiGet::onNetworkError(QNetworkReply::NetworkError code, QString strError)
{
    errThreadCount++;
    emit downloadProgress(bytesDone, fileSize, percent, speed);
    emit error(QString::number((int)code) + " " + strError);
    qDebug() << "Error: " + strError;
    if (errThreadCount >= parts.size())
    {

        if (parts.at(0))
            if (criticalErrorSet.contains(static_cast<QNetworkReply::NetworkError>(parts.at(0)->getLastErrCode())))
            {
                stop();
            }
        setState(dfError);
    }
}

void HttpMultiGet::onFinished()
{
    QPointer<HttpGet> downloader = qobject_cast<HttpGet*>(sender());
    if (!downloader)
        return;

    if (downloader->gotAll())
    {
        finishedThreadCount++;
        qDebug() << trUtf8("Секция ") + downloader->getFileName() + trUtf8(" закончена.");
    }
    else
        qDebug() << trUtf8("Секция ") + downloader->getFileName() + trUtf8(" остановлена.");

    lastTimePr = QTime(0, 0);
    onDownloadProgress(bytesDone, fileSize, percent, speed);

//    qDebug() << "finishedThreadCount = " + QString::number(finishedThreadCount) +
//                " of " + QString::number(threadCount);

    if (finishedThreadCount >= threadCount)//parts.size())
    {
        timer.stop();
        timeCounter.stop();
//        emit downloadProgress(bytesDone, fileSize, percent, speed, &parts);        
//        speed = 0;                
        concatFiles();
        setState(dfDone);
        clearParts();
        emit allFinished();
    }
}

void HttpMultiGet::onDownloadProgress(qint64 gotBytes, qint64 totalBytes, uchar percent, int speed)
{
    Q_UNUSED(gotBytes);
    Q_UNUSED(totalBytes);
    Q_UNUSED(percent);
    Q_UNUSED(speed);

    HttpGet* download = qobject_cast<HttpGet*>(sender());

    curTimePr = QTime::currentTime();
    if (download->getIdleIntervals() < IDLE_TIMEOUT_INTERVALS) //нормальный процесс, не затормозился, иначе надо сообщить
    {
        if (abs(lastTimePr.msecsTo(curTimePr)) < PROGRESS_INTERVAL)
            return;
        lastTimePr = curTimePr;
    }

    bytesDone = 0;
    this->percent = 0;
    uint pcent = 0;
    this->speed = 0;    

    foreach (QPointer<HttpGet> part, parts)
    {
        bytesDone += part->getBytesReceived();
        pcent += part->getPercent();
        if (part->isDownloading())
        {
            this->speed += part->getSpeed();
        }    
    }

    this->percent = pcent / threadCount;//parts.size();

    emit downloadProgress(bytesDone, fileSize, this->percent, this->speed);
}

void HttpMultiGet::onLocalError(QString errMsg)
{
    errThreadCount++;
    emit error("-1 " + errMsg);
/*    if (errThreadCount >= parts.size())
    {
        setState(dfError);
    }
    if (parts.isEmpty() || (qobject_cast<HttpGet*>(sender()) == parts.at(0))) //ошибка файла/каталога в основной секции
    */
    {
        stop();
        setState(dfError);
    }
}

void HttpMultiGet::onReplyCode(int code, QString msg)
{
    //хз, может, не нужно
    emit replyCode(code, msg);
    qDebug() << "Reply: " + msg;
}

void HttpMultiGet::onTimeCounter()
{
    timeElapsed++;
    emit timeCounterSignal(timeElapsed);
}

void HttpMultiGet::onAbortTimer()
{
    if (starting)
        return;
    uchar stoppedCount = 0;
    foreach (QPointer<HttpGet> downloader, parts)
    {
        if (downloader && !downloader->isAborting())
            downloader->abortDownload();
        stoppedCount++;
    }
    if (stoppedCount >= threadCount)
    {
//        qDebug() << QString("--stoppedCount = %1, threads = %2").arg(stoppedCount).arg(threadCount);
        abortTimer.stop();
        timeCounter.stop();
        if (state == dfDownloading)
            setState(dfPaused);
        emit stopped();
    }
}

void HttpMultiGet::onTimer()
{
    if (state == dfPaused)
        return;
    foreach (QPointer<HttpGet> part, parts)
    {
        //перезапуск остановленных
        if ((!part->isDownloading()) && (!part->gotAll()) && (!part->hasStarted()))
        {
            qDebug() << trUtf8("Перезапуск секции с ") + part->getFileName();
            if (part->reGetFile())
                setState(dfDownloading);
            if (errThreadCount > 0)
                errThreadCount--;
        }
    }
}

void HttpMultiGet::onRedirectToURL(QString newURL)
{
    URL = newURL;
    qDebug() << "---Redirect---";
}

void HttpMultiGet::clearParts()
{
//    foreach (HttpGet* dLoader, parts)
//    {
//        if (dLoader)
//        {
//            dLoader->deleteLater();
//            dLoader = 0;
//        }
//    }
    uchar s = parts.size();
    for(uchar i=0; i<s; i++)
    {
        if (parts[i])
        {
            parts[i]->deleteLater();
            parts[i] = 0;
        }
    }
    parts.clear();
}

void HttpMultiGet::deleteFileParts()
{    
    qDebug() << "Delete files.";
    QFile file;
    timeElapsed = 0;

    if (parts.isEmpty() && (!localPath.isEmpty())) //удалить файлы все равно
    {
        QFileInfo fi(localPath);
        QDir dir(fi.absoluteDir());
        QStringList filterList;
        filterList.append("." + fi.fileName() + ".part*");
        dir.setNameFilters(filterList);
        QStringList fileList = dir.entryList(QDir::Files | QDir::Hidden);
        foreach (QString fName, fileList)
        {
            qDebug() << "Delete file: " << dir.absolutePath() + "/" + fName;
            file.setFileName(dir.absolutePath() + "/" + fName);
            file.remove();
        }
        return;
    }

    foreach (QPointer<HttpGet> downloader, parts)
    {
        if (downloader)
        {
            file.setFileName(downloader->getFilePath());
            file.remove();
            delete downloader;
        }
    }
    parts.clear();
}

void HttpMultiGet::updateState()
{
    if (parts.isEmpty())
        return;

    int downloadCount = 0;
    int doneCount = 0;
    int errCount = 0;

    int partCount = parts.size();

    foreach (HttpGet* part, parts)
    {
        if (part->isDownloading())
            downloadCount++;
        else
            if (part->getLastErrCode())
                errCount++;
        if (part->gotAll())
            doneCount++;
    }

    if (downloadCount)
    {
        setState(dfDownloading);
        return;
    }

    if (doneCount == partCount)
    {
        setState(dfDone);
        return;
    }

    if (errCount)
        setState(dfError);
    else
        setState(dfPaused);
}

void HttpMultiGet::startTimers()
{
    timer.start();
    timeCounter.start();
}

void HttpMultiGet::stopTimers()
{
    timer.stop();
    timeCounter.stop();
}

void HttpMultiGet::concatFiles()
{
    if (parts.isEmpty())
        return;
    qDebug() << trUtf8("Склеиваем части.");
    QFileInfo fi(localPath);
    QFile resFile(fi.absoluteFilePath());//т.к. хер знает, какие слеши будут. а тут такие "/"
    QIODevice::OpenMode flags = QIODevice::WriteOnly | QIODevice::Truncate;

    QFileInfo pFi;
    QFile pFile;
//    char *buff[BUFF_SIZE];
    QByteArray buff;
//    qint64 readCount;

    if (resFile.open(flags))
    {
        foreach (QPointer<HttpGet> part, parts)
        {
            pFi.setFile(part->getFilePath());
            pFile.setFileName(pFi.absoluteFilePath());
            if (pFile.open(QIODevice::ReadOnly))
            {
                while (!(buff = pFile.read(BUFF_SIZE)).isEmpty())
                {
                    resFile.write(buff);
                }

                pFile.close();
                pFile.remove(); //на крайняк все потом удалять
            }
        }
        resFile.close();
    }
}



