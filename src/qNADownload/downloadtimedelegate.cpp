#include "downloadtimedelegate.h"
#include <QPainter>
#include <QApplication>

DownloadTimeDelegate::DownloadTimeDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{

}



void DownloadTimeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (!index.isValid())
        return;
    uint secs = index.data().toUInt();

    uint days = secs / 86400;
    secs -= days * 86400;
    uint hours = secs / 3600;
    secs -= hours * 3600;
    uint minutes = secs / 60;
    secs -= minutes * 60;

    QString text("");

    if (days > 9)
    {
        text = trUtf8("вечность");
    }
    else
    {
        if (days)
            text.append(QString(trUtf8("%1 д ")).arg(days));
        if (hours)
        {
            if (hours < 10)
                text.append("0");
            text.append(QString("%1:").arg(hours));
        }
        if (minutes < 10)
            text.append("0");
        text.append(QString("%1:").arg(minutes));
        if (secs < 10)
            text.append("0");
        text.append(QString("%1").arg(secs));
    }


    QPen pPen = painter->pen();
    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(option.rect, option.palette.highlight());
        painter->setPen(option.palette.highlightedText().color());

    }
    else
    {
        painter->setPen(option.palette.text().color());
    }

    QRect rect(option.rect);
    //rect.adjust(iSize + 5, 0, 0, 0);

    QApplication::style()->drawItemText(painter, rect, Qt::AlignHCenter | Qt::AlignVCenter,
                                        option.palette, true, text);
    painter->setPen(pPen);
}

QSize DownloadTimeDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    return QSize(70, option.rect.height());
}
