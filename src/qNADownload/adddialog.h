#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QModelIndex>
#include "global.h"
#include <functional>

/*
template <typename T>
class CheckDelegate
{
public:
    CheckDelegate(T *obj, bool (T::*checkFn)(QString))
    {
        object = obj;
        checkFunction = checkFn;
    }
    virtual ~CheckDelegate();
    bool operator() (QString checkStr)
    {
        return object->*checkFunction(checkStr);
    }
private:
    T* object;
    bool (T::*checkFunction)(QString);
};
*/

typedef std::function<bool(QString, QModelIndex)> CheckDelegate;

namespace Ui {
class AddDialog;
}


class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0, CheckDelegate fileExistsDelegate = 0, QModelIndex selected = QModelIndex());
    ~AddDialog();
    void setURL(const QString URL);
    void getResults(QUrl &URL, QString &localPath, uchar &threadCount, bool &runImmediate);
private:
    Ui::AddDialog *ui;    
    CheckDelegate fileExists;
    QModelIndex selectedIndex;
    // QDialog interface
public slots:
    int exec();
    virtual int exec(const QString initialDir);
    virtual int exec(const QString URL, const QString localPath, const uchar threads = DEF_THREAD_COUNT,
                     const bool isRunImmediateVisible = true);

private slots:
    void on_toolButton_clicked();
    void on_leURL_editingFinished();
    void onAccepted();

protected:

};

#endif // ADDDIALOG_H
