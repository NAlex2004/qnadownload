#-------------------------------------------------
#
# Project created by QtCreator 2015-05-17T10:11:52
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qNADownload
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    downloadtablemodel.cpp \
    downloadprogressdelegate.cpp \
    downloadnamedelegate.cpp \
    downloadsortfiltermodel.cpp \
    adddialog.cpp \
    httpmultiget.cpp \
    httpget.cpp \
    downloadtimedelegate.cpp \
    downloadsectionsmodel.cpp \
    SingleApplication.cpp

HEADERS  += mainwindow.h \
    downloadtablemodel.h \
    downloadprogressdelegate.h \
    downloadfiledata.h \
    downloadnamedelegate.h \
    downloadsortfiltermodel.h \
    adddialog.h \
    global.h \
    httpmultiget.h \
    httpget.h \
    downloadtimedelegate.h \
    downloadsectionsmodel.h \
    SingleApplication.h

FORMS    += mainwindow.ui \
    adddialog.ui

RESOURCES += \
    icons.qrc

RC_FILE = "qNADownload.rc"
