#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include <QObject>
#include <QApplication>
#include <QtNetwork/QLocalServer>
#include <QWidget>

class SingleApplication : public QApplication {
         Q_OBJECT
     public:
         SingleApplication(int &argc, char **argv);

         bool isRunning();                // Are there instances running
         QWidget *w;                        // The MainWindow pointer

     private slots:
         // A trigger new connections
         void _newLocalConnection();

     private:
         // Initialize the local connection
         void _initLocalConnection();
         // Create the server
         void _newLocalServer();
         // Activates the window
         void _activateWindow();

         bool _isRunning;                // Are there instances running
         QLocalServer *_localServer;     // The local socket Server
         QString _serverName;            // Service name
 };

#endif // SINGLEAPPLICATION_H
