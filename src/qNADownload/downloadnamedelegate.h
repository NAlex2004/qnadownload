#ifndef DOWNLOADNAMEDELEGATE_H
#define DOWNLOADNAMEDELEGATE_H

#include <QStyledItemDelegate>
#include <QList>


class QPainter;
class QImage;

class DownloadNameDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit DownloadNameDelegate(QObject *parent = 0);

signals:

public slots:


    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:
    QList<QImage> icons;

};

#endif // DOWNLOADNAMEDELEGATE_H
