#include "downloadsortfiltermodel.h"
#include "downloadtablemodel.h"


DownloadSortFilterModel::DownloadSortFilterModel(QObject *parent):
    QSortFilterProxyModel(parent)
{
    clearFilter();    
}

DownloadSortFilterModel::~DownloadSortFilterModel()
{

}

bool DownloadSortFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    Q_UNUSED(source_parent);

    DFState state = static_cast<DFState>(sourceModel()->index(source_row, STATE_COLUMN, source_parent).data().toUInt());
            //static_cast<DownloadTableModel*>(sourceModel())->getRowData(source_row)->getState();
    return filterSet.contains(state);
    //return QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent);
}

bool DownloadSortFilterModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    switch (left.column())
    {
    case 0://filename
        return (QString::compare(left.data().toString(), right.data().toString(), Qt::CaseInsensitive) < 0);
    case 3://percent
        return (left.data().toUInt() < right.data().toUInt());
    }

    return QSortFilterProxyModel::lessThan(left, right);
}

DFStateSet DownloadSortFilterModel::getFilterSet() const
{
    return filterSet;
}

void DownloadSortFilterModel::setFilterSet(const DFStateSet &value)
{
    filterSet = value;
    invalidateFilter();
}

void DownloadSortFilterModel::clearFilter()
{
    filterSet.insert(dfUnknown);
    filterSet.insert(dfDone);
    filterSet.insert(dfDownloading);
    filterSet.insert(dfError);
    filterSet.insert(dfPaused);
}

DownloadFileData *DownloadSortFilterModel::getRowData(int sourceRow) const
{
    return static_cast<DownloadTableModel*>(sourceModel())->getRowData(sourceRow);
}


