#include "mainwindow.h"
//#include <QApplication>
#include "SingleApplication.h"
#include <iostream>
//#include <QPixmap>

int main(int argc, char *argv[])
{
//    QApplication a(argc, argv);
    SingleApplication a(argc, argv);
    //a.setWindowIcon(QPixmap(":/res/qNADownload.png"));
    if (!a.isRunning())
    {
        MainWindow w;
        a.w = &w;
        //a.setStyleSheet("QStatusBar::item { border: 0px solid black }; ");
        w.show();

        return a.exec();
    }

    return 0;
}
