#include "downloadnamedelegate.h"
#include "downloadfiledata.h"
#include "downloadtablemodel.h"
#include <QApplication>
#include <QPainter>
#include <QImage>
//#include <QPalette>

DownloadNameDelegate::DownloadNameDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    icons << QImage(":/res/unknown-16.png")
                     << QImage(":/res/running-16.png")
                     << QImage(":/res/finished-16.png")
                     << QImage(":/res/error-16.png")
                     << QImage(":/res/pause-16.png");

}


void DownloadNameDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    //QBrush br = option.palette.background();
    if (!index.isValid())
        return;
    //painter->fillRect(option.rect, option.palette.background());
    QString text = index.data().toString();
    //painter->drawText();
    //сначала значок, потом текст... может и без drawItemText

    QPen pPen = painter->pen();
    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(option.rect, option.palette.highlight());
        painter->setPen(option.palette.highlightedText().color());
    }
    else
    {
        painter->setPen(option.palette.text().color());
    }    

    const uint state = index.model()->index(index.row(), STATE_COLUMN).data().toUInt();
    //const DownloadFileData *fData = static_cast<const DownloadTableModel*>(index.model())->getRowData(index.row());
    //int iSize = icons[fData->getState()].rect().height();
    int iSize = icons.at(state).rect().height();

    QRect icRect(option.rect.left() + 1,
                 option.rect.top() + (option.rect.height() - iSize) / 2, iSize, iSize);


    painter->drawImage(icRect, icons[state], icons[state].rect());

    QRect rect(option.rect);
    rect.adjust(iSize + 5, 0, 0, 0);

    QApplication::style()->drawItemText(painter, rect, Qt::AlignLeft | Qt::AlignVCenter,
                                        option.palette, true, text);
    painter->setPen(pPen);
}

QSize DownloadNameDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    //return QSize(option.rect.width(), option.rect.height());
    return QSize(180, option.rect.height());
}
