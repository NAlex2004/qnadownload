#ifndef DOWNLOADTABLEMODEL_H
#define DOWNLOADTABLEMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QPointer>
#include "downloadfiledata.h"
#include "global.h"



class DownloadTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit DownloadTableModel(QObject *parent = 0);
    ~DownloadTableModel();
signals:

public slots:
    void rowDataChanged();
    void timeChanged();
    void stateChanged();

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    virtual bool appendRow(const QPointer<DownloadFileData> fileData);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual void setRowData(int row, QPointer<DownloadFileData> const fileData);
    virtual QPointer<DownloadFileData>  getRowData(int row) const;
protected:
    const QString headers[COL_COUNT] = {trUtf8("Имя"), trUtf8("Получено"), trUtf8("Размер"), trUtf8("%"),
                                trUtf8("Скорость"), trUtf8("Прошло"), trUtf8("Осталось")};
    QList<QPointer<DownloadFileData>> fileList;

};

#endif // DOWNLOADTABLEMODEL_H
