#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QLabel>
#include <QPointer>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QItemSelection>
#include "downloadprogressdelegate.h"
#include "downloadtablemodel.h"
#include "downloadfiledata.h"
#include "downloadsectionsmodel.h"
#include "global.h"

const QString TITLE = QObject::trUtf8("qNADownload (менеджер закачек)");
const QString APPNAME = QObject::trUtf8("qNADownload");
const QString ORGNAME = QObject::trUtf8("AlienSoft");
const QString ABOUT = QObject::trUtf8("qNADownload - менеджер закачек.\nВерсия: 0.1\nАвтор: Алексей 'AlieN' Назаров.");

class DownloadSortFilterModel;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_filterList_currentRowChanged(int currentRow);    
    void on_toolBarToggleAction_toggled(bool arg1);
    void downloadView_currentRowChanged(const QModelIndex & current, const QModelIndex & previous);
    void downloadView_selectionChanged(QItemSelection selected, QItemSelection deselected);

    void on_aboutAction_triggered();

    void on_aboutQtAction_triggered();

    void on_deleteAction_triggered();

    void on_addAction_triggered();

    void on_deleteAllAction_triggered();

    void on_editAction_triggered();

    void on_downloadView_doubleClicked(const QModelIndex &index);

    void on_runAction_triggered();

    void on_pauseAction_triggered();

    void on_restartAction_triggered();

    void on_stopAllAction_triggered();

    void updateSectionDetails();
    void onStateChanged();
    void onTimer();

    void on_runAllAction_triggered();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void showAsItWas();
    void onDownloadFinished();    

private:
    static const uint TIMER_INTERVAL = 1000;
    bool active;

    Ui::MainWindow *ui;
    DownloadTableModel* downloadModel;
    DownloadSortFilterModel* filterModel;
    DownloadSectionsModel* sectionsModel;
    QTimer timer;
    QLabel statusLabel;
    QSystemTrayIcon* trayIcon;

    void refreshActionsByIndex(const QModelIndex &ind);
    void setViewColumnWidth();
    void setContextMenus();
    void connectToModel(QPointer<DownloadFileData> signalSender);
    bool confirmDelete(const QString &dialogText, bool &deleteFileAlso);
    QModelIndex getIndexByProxy(const QModelIndex &proxyIndex) const;
    QPointer<DownloadFileData> getFileDataByProxy(const QModelIndex &proxyIndex) const;
    void saveSettings();
    void restoreSettings();
    void createTrayIcon();
    bool fileOrDownloadExists(QString path, QModelIndex currIndex);
    QString getURLFromClipboard();

    // QWidget interface
protected:
    void changeEvent(QEvent *evt);

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
