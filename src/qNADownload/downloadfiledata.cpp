#include "downloadfiledata.h"
#include <QFileInfo>

const QString DownloadFileData::strStates[5] = {QObject::trUtf8("неизвестно"), QObject::trUtf8("скачивается"),
                                         QObject::trUtf8("завершена"), QObject::trUtf8("ошибка"),
                                         QObject::trUtf8("остановлена")};

DownloadFileData::DownloadFileData(QString URLPath, QString localFile):
    localPath(localFile), URL(URLPath), percent(0), fileSize(0), bytesDone(0), threadCount(1),
    timeElapsed(0, 0), timeLeft(0, 0), state(dfUnknown), speed(0)
{
}

DownloadFileData::~DownloadFileData()
{

}

QString DownloadFileData::getLocalPath() const
{
    return localPath;
}

void DownloadFileData::setLocalPath(const QString &value)
{
    localPath = value;
}

QString DownloadFileData::getFileName() const
{
    QFileInfo FInfo(localPath);
    //if (FInfo.exists())
    {
      return FInfo.fileName();
    }
    //return QString(QObject::trUtf8("нет файла"));
}

uchar DownloadFileData::getPercent() const
{
    return percent;
}

void DownloadFileData::setPercent(uchar value)
{
    percent = value;
}

qint64 DownloadFileData::getFileSize() const
{
    return fileSize;
}

qint64 DownloadFileData::getBytesDone() const
{
    return bytesDone;
}

uchar DownloadFileData::getThreadCount() const
{
    return threadCount;
}

QTime DownloadFileData::getTimeElapsed() const
{
    return timeElapsed;
}

QTime DownloadFileData::getTimeLeft() const
{
    return timeLeft;
}

DFState DownloadFileData::getState() const
{
    return state;
}

QString DownloadFileData::getStateStr() const
{
    return strStates[state];
}

void DownloadFileData::setState(DFState value)
{
    state = value;
}

float DownloadFileData::getSpeed() const
{
    return speed;
}

QString DownloadFileData::getURL() const
{
    return URL;
}

void DownloadFileData::setURL(const QString &value)
{
    URL = value;
}






